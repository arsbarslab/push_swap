/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stack.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ttreutel <ttreutel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/27 17:32:54 by ttreutel          #+#    #+#             */
/*   Updated: 2019/01/28 23:11:37 by ttreutel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"
#include <stdlib.h>

void	ft_print_stack(t_stack *stack, t_stack *stack_b)
{
	int num_a;
	int num_b;

	while (stack || stack_b)
	{
		if (!stack)
		{
			num_b = stack_b->num;
			ft_putstr("\t");
			ft_putstr(ft_itoa(num_b));
			ft_putstr("\n");
			stack_b = stack_b->next;
		}
		else if (!stack_b)
		{
			num_a = stack->num;
			ft_putstr(ft_itoa(num_a));
			ft_putstr("\t");
			ft_putstr("\n");
			stack = stack->next;
		}
		else
			ft_print_all(&stack, &stack_b);
	}
	ft_putstr("\n");
}

int		ft_len_stack(t_stack *head)
{
	int	len;

	len = 0;
	while (head)
	{
		head = head->next;
		len++;
	}
	return (len);
}

t_stack	*ft_create_stack_from_arg(int argc, char **argv, int flag)
{
	t_stack	*head;
	int		i;

	argc = 0;
	if (!flag)
		argv = ft_strsplit(argv[1], ' ');
	else
		argv = ft_strsplit(argv[2], ' ');
	while (argv[argc])
		argc++;
	head = ft_create_stack(argc, argv, 1);
	i = 0;
	while (i < argc)
	{
		ft_strdel(&(argv[i]));
		i++;
	}
	free(argv);
	return (head);
}

t_stack	*ft_load_stack(int argc, char **argv)
{
	if (argc == 2)
		return (ft_create_stack_from_arg(argc, argv, 0));
	else if (argc == 3 && ft_cv(argv))
		return (ft_create_stack_from_arg(argc, argv, 1));
	else
		return (ft_create_stack(argc, argv, 0));
}

t_stack	*ft_create_stack(int argc, char **argv, int type)
{
	int		i;
	t_stack	*head;
	t_stack	*prev;
	t_stack	*new;

	if (ft_check_num(argc, argv) && ft_check_dup(argc, argv)
		&& ft_is_bigger_int(argc, argv))
	{
		prev = ft_stnew(ft_atoi(argv[type ? 0 : 1]));
		head = prev;
		i = type ? 1 : 2;
		while (i < argc)
		{
			new = ft_stnew(ft_atoi(argv[i]));
			prev->next = new;
			prev = new;
			i++;
		}
		return (head);
	}
	return (NULL);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ttreutel <ttreutel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/27 17:36:11 by ttreutel          #+#    #+#             */
/*   Updated: 2019/01/28 23:11:37 by ttreutel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_PUSH_SWAP_H
# define PUSH_SWAP_PUSH_SWAP_H
# include "libft/libft.h"
# include <stdlib.h>

typedef struct		s_stack
{
	int				num;
	struct s_stack	*next;
}					t_stack;

int					ft_check_num(int argc, char **argv);
int					ft_is_bigger_int(int argc, char **argv);
int					ft_check_dup(int argc, char **argv);

t_stack				*ft_stnew(int num);
void				ft_stiter(t_stack *stack, void (*f)(t_stack *elem));
int					ft_len_stack(t_stack *head);
int					ft_get_num(t_stack *head, int index);
int					ft_get_closest(t_stack *head, int num);
int					ft_check_pos(t_stack *head, int num, int index);
int					ft_get_steps(t_stack *head, int num);
int					ft_get_steps_ab(t_stack *head, int num, int *type);
int					ft_get_pos_in_a(t_stack *head, int num);
void				ft_print_stack(t_stack *stack, t_stack *stack_b);
int					ft_ab_steps(t_stack *a, t_stack *b, int num);
void				ft_move_in_a(t_stack **a, t_stack **b, int num);
void				ft_move_ab(t_stack **a, t_stack **b);
void				ft_move_stack_a(t_stack **head, int type, int count);
void				ft_to_begin(t_stack **a);
void				ft_solve(t_stack **head_a, t_stack **head_b);
t_stack				*ft_load_stack(int argc, char **argv);
t_stack				*ft_create_stack(int argc, char **argv, int type);
int					ft_is_sorted(t_stack *head);
void				ft_solve_mini(t_stack **a);

void				ft_swap(t_stack **head);
void				ft_sa(t_stack **head);
void				ft_sb(t_stack **head);
void				ft_ss(t_stack **head_a, t_stack **head_b);
void				ft_pa(t_stack **head_b, t_stack **head_a);
void				ft_pb(t_stack **head_b, t_stack **head_a);
void				ft_rotate(t_stack **head);
void				ft_ra(t_stack **head);
void				ft_rb(t_stack **head);
void				ft_rr(t_stack **head_a, t_stack **head_b);
void				ft_rev_rotate(t_stack **head);
void				ft_rra(t_stack **head);
void				ft_rrb(t_stack **head);
void				ft_rrr(t_stack **head_a, t_stack **head_b);

void				ft_do_pb(t_stack **head_b, t_stack **head_a);
void				ft_do_pa(t_stack **head_b, t_stack **head_a);
void				ft_do_rr(t_stack **head_a, t_stack **head_b);
void				ft_do_ss(t_stack **a, t_stack **b, int count, int type);
void				ft_do_ss_ch(t_stack **head_b, t_stack **head_a);
void				ft_clean(t_stack *head);
void				ft_do_rrr(t_stack **head_a, t_stack **head_b);
int					ft_cv(char **argv);
int					ft_is_correct(char *op);
void				ft_do_cmd(int cmd, t_stack **a, t_stack **b);
void				ft_error(void);
void				ft_print_all(t_stack **a, t_stack **b);
#endif

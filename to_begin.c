/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   to_begin.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ttreutel <ttreutel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/27 17:34:02 by ttreutel          #+#    #+#             */
/*   Updated: 2019/01/28 23:11:37 by ttreutel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int		ft_get_min(t_stack *a)
{
	int		num;
	t_stack	*tmp;

	num = 2147483647;
	tmp = a;
	while (tmp)
	{
		if (tmp->num < num)
			num = tmp->num;
		tmp = tmp->next;
	}
	return (num);
}

int		ft_get_steps_begin(t_stack *head, int index, int *type)
{
	int len;

	len = ft_len_stack(head);
	if (index > len / 2)
	{
		*type = 1;
		return (len - index);
	}
	else
	{
		*type = 0;
		return (index);
	}
}

void	ft_to_begin(t_stack **a)
{
	int index;
	int type;
	int steps;

	type = -2;
	index = ft_get_closest(*a, ft_get_min(*a));
	steps = ft_get_steps_begin(*a, index, &type);
	ft_move_stack_a(a, type, steps);
}

int		ft_is_sorted(t_stack *head)
{
	t_stack	*tmp;
	int		prev;

	tmp = head;
	prev = head->num;
	while (tmp)
	{
		prev = tmp->num;
		head = tmp;
		while (head)
		{
			if (prev > head->num)
				return (0);
			head = head->next;
		}
		tmp = tmp->next;
	}
	return (1);
}

void	ft_solve_mini(t_stack **a)
{
	if ((*a)->num > (*a)->next->num)
	{
		if ((*a)->next->num < (*a)->next->next->num)
		{
			if ((*a)->next->next->num < (*a)->num)
				ft_ra(a);
			else
				ft_sa(a);
		}
		else
			ft_sa(a);
	}
	else
	{
		if ((*a)->num > (*a)->next->next->num)
			ft_rra(a);
		else
		{
			ft_ra(a);
			ft_sa(a);
		}
	}
	ft_to_begin(a);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ttreutel <ttreutel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/27 17:36:03 by ttreutel          #+#    #+#             */
/*   Updated: 2019/01/28 23:11:36 by ttreutel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	ft_move_stack_b(t_stack **head, int type, int count)
{
	if (type == 1)
	{
		while (count--)
			ft_rrb(head);
	}
	else if (type == 0)
	{
		while (count--)
			ft_rb(head);
	}
}

int		ft_get_mix_steps(int sa, int sb)
{
	int mix;
	int more;

	if (sa >= sb)
	{
		more = sa - sb;
		mix = sa - more;
		return (mix);
	}
	else
	{
		more = sb - sa;
		mix = sb - more;
		return (mix);
	}
}

void	ft_do_ab_steps(t_stack **a, t_stack **b, int num)
{
	int steps_b;
	int steps_a;
	int type_a;
	int type_b;
	int mix;

	type_a = -1;
	type_b = -2;
	steps_b = ft_get_steps_ab(*b, num, &type_b);
	steps_a = ft_get_steps_ab(*a, num, &type_a);
	if (type_a == type_b)
	{
		mix = ft_get_mix_steps(steps_a, steps_b);
		ft_do_ss(a, b, mix, type_a);
		ft_move_stack_a(a, type_a, steps_a - mix);
		ft_move_stack_b(b, type_b, steps_b - mix);
		ft_pa(b, a);
	}
	else
	{
		ft_move_stack_a(a, type_a, steps_a);
		ft_move_stack_b(b, type_b, steps_b);
		ft_pa(b, a);
	}
}

int		ft_index_b_mix_steps(t_stack *head_a, t_stack *head_b)
{
	int		a_steps;
	int		tmp;
	int		total;
	int		index_b;
	t_stack	*h;

	a_steps = ft_get_steps(head_a, head_b->num);
	total = a_steps;
	index_b = 0;
	h = head_b;
	while (head_b)
	{
		tmp = ft_ab_steps(head_a, h, head_b->num);
		if (total > tmp)
		{
			total = tmp;
			index_b = ft_get_pos_in_a(h, head_b->num);
		}
		head_b = head_b->next;
	}
	return (index_b);
}

void	ft_move_ab(t_stack **a, t_stack **b)
{
	int index_b;

	index_b = ft_index_b_mix_steps(*a, *b);
	ft_do_ab_steps(a, b, ft_get_num(*b, index_b));
}

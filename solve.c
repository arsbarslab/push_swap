/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ttreutel <ttreutel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/27 17:36:28 by ttreutel          #+#    #+#             */
/*   Updated: 2019/01/28 23:11:36 by ttreutel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int		ft_get_steps(t_stack *head, int num)
{
	int index;
	int len;

	len = ft_len_stack(head);
	index = ft_get_pos_in_a(head, num);
	if (index > len / 2)
		return (len - index);
	else
		return (index);
}

int		ft_get_steps_ab(t_stack *head, int num, int *type)
{
	int index;
	int len;

	len = ft_len_stack(head);
	index = ft_get_pos_in_a(head, num);
	if (index > len / 2)
	{
		*type = 1;
		return (len - index);
	}
	else
	{
		*type = 0;
		return (index);
	}
}

int		ft_ab_steps(t_stack *a, t_stack *b, int num)
{
	int steps_b;
	int steps_a;
	int type_a;
	int type_b;
	int total;

	type_a = -1;
	type_b = -2;
	steps_b = ft_get_steps_ab(b, num, &type_b);
	steps_a = ft_get_steps_ab(a, num, &type_a);
	if (type_a == type_b)
	{
		total = steps_a >= steps_b ? steps_a : steps_b;
	}
	else
		total = steps_a + steps_b;
	return (total);
}

int		ft_mix_steps(t_stack *head_a, t_stack *head_b)
{
	int		a_steps;
	int		tmp;
	int		total;
	t_stack	*head_b_fix;

	head_b_fix = head_b;
	a_steps = ft_get_steps(head_a, head_b->num);
	total = a_steps;
	while (head_b)
	{
		tmp = ft_ab_steps(head_a, head_b_fix, head_b->num);
		if (total > tmp)
			total = tmp;
		head_b = head_b->next;
	}
	return (total);
}

void	ft_solve(t_stack **head_a, t_stack **head_b)
{
	int	steps_a;
	int	steps_a_b;

	steps_a = ft_get_steps(*head_a, (*head_b)->num);
	steps_a_b = ft_mix_steps(*head_a, *head_b);
	if (steps_a <= steps_a_b)
	{
		ft_move_in_a(head_a, head_b, (*head_b)->num);
	}
	else
	{
		ft_move_ab(head_a, head_b);
	}
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_pos.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ttreutel <ttreutel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/28 23:09:20 by ttreutel          #+#    #+#             */
/*   Updated: 2019/01/28 23:10:36 by ttreutel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	ft_move_in_a(t_stack **a, t_stack **b, int num)
{
	int i;
	int type;
	int steps;

	steps = ft_get_steps_ab(*a, num, &type);
	i = 0;
	while (i < steps)
	{
		if (type == 1)
			ft_rra(a);
		else
			ft_ra(a);
		i++;
	}
	ft_pa(b, a);
}

void	ft_do_ss(t_stack **a, t_stack **b, int count, int type)
{
	if (type == 1)
	{
		while (count--)
			ft_rrr(a, b);
	}
	else if (type == 0)
	{
		while (count--)
			ft_rr(a, b);
	}
}

void	ft_move_stack_a(t_stack **head, int type, int count)
{
	if (type == 1)
	{
		while (count--)
			ft_rra(head);
	}
	else if (type == 0)
	{
		while (count--)
			ft_ra(head);
	}
}

int		ft_get_pos_in_a(t_stack *head, int num)
{
	int index;

	index = ft_get_closest(head, num);
	index = ft_check_pos(head, num, index);
	return (index);
}

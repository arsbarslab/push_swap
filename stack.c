/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ttreutel <ttreutel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/28 23:03:43 by ttreutel          #+#    #+#             */
/*   Updated: 2019/01/28 23:05:35 by ttreutel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	ft_clean(t_stack *head)
{
	free(head);
}

t_stack	*ft_stnew(int num)
{
	t_stack *new;

	new = (t_stack *)malloc(sizeof(t_stack));
	if (!new)
		return (NULL);
	new->num = num;
	new->next = NULL;
	return (new);
}

void	ft_stiter(t_stack *stack, void (*f)(t_stack *elem))
{
	t_stack	*next;
	t_stack	*current;

	if (stack && f)
	{
		next = stack->next;
		current = stack;
		while (current)
		{
			next = current->next;
			f(current);
			current = next;
		}
	}
}

void	ft_print_all(t_stack **a, t_stack **b)
{
	int num_a;
	int num_b;

	num_a = (*a)->num;
	num_b = (*b)->num;
	ft_putstr(ft_itoa(num_a));
	ft_putstr("\t");
	ft_putstr(ft_itoa(num_b));
	ft_putstr("\n");
	*a = (*a)->next;
	*b = (*b)->next;
}

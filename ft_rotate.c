/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rotate.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ttreutel <ttreutel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/27 17:42:17 by ttreutel          #+#    #+#             */
/*   Updated: 2019/01/28 23:11:37 by ttreutel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	ft_ra(t_stack **head)
{
	ft_rotate(head);
	ft_putstr("ra\n");
}

void	ft_rb(t_stack **head)
{
	ft_rotate(head);
	ft_putstr("rb\n");
}

void	ft_rr(t_stack **head_a, t_stack **head_b)
{
	ft_rotate(head_a);
	ft_rotate(head_b);
	ft_putstr("rr\n");
}

void	ft_rra(t_stack **head)
{
	ft_rev_rotate(head);
	ft_putstr("rra\n");
}

void	ft_rrb(t_stack **head)
{
	ft_rev_rotate(head);
	ft_putstr("rrb\n");
}

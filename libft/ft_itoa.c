/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ttreutel <ttreutel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/27 19:01:56 by ttreutel          #+#    #+#             */
/*   Updated: 2019/01/28 23:11:37 by ttreutel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static size_t	ft_num_len(long long int n)
{
	size_t	len;

	len = 0;
	if (n < 0)
		++len;
	while (n)
	{
		n /= 10;
		++len;
	}
	return (len);
}

static void		ft_itoa_inner(long long int n, size_t len, char *buf)
{
	char	*ptr;

	ptr = buf;
	while (len)
	{
		ptr[len - 1] = n % 10 + '0';
		n /= 10;
		--len;
	}
}

char			*ft_itoa(long long int n)
{
	char			*str;
	size_t			len;
	int				sign;

	sign = n >= 0 ? 1 : -1;
	len = ft_num_len(n);
	if (n == 0)
		len = 1;
	str = (char *)malloc(sizeof(char) * (len + 1));
	if (!str)
		return (NULL);
	if (n < -9223372036854775807)
	{
		ft_strcpy(str, "-9223372036854775808");
		return (str);
	}
	n *= sign;
	ft_itoa_inner(n, len, str);
	if (sign == -1)
		str[0] = '-';
	str[len] = '\0';
	return (str);
}

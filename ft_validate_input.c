/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_validate_input.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ttreutel <ttreutel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/27 17:35:33 by ttreutel          #+#    #+#             */
/*   Updated: 2019/01/28 23:11:37 by ttreutel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int		ft_check_num(int argc, char **argv)
{
	int i;
	int j;
	int len;

	i = 1;
	while (i < argc)
	{
		len = ft_strlen(argv[i]);
		j = 0;
		while (argv[i][j] && j < len)
		{
			if (j == 0 && argv[i][j] == '-' && len > 1)
				j++;
			if (!ft_isdigit(argv[i][j]))
				return (0);
			j++;
		}
		i++;
	}
	return (1);
}

int		ft_is_bigger_int(int argc, char **argv)
{
	int max;
	int min;
	int i;

	max = 2147483647;
	min = -2147483648;
	i = 1;
	while (i < argc)
	{
		if (ft_atoi(argv[i]) > max || ft_atoi(argv[i]) < min)
			return (0);
		i++;
	}
	return (1);
}

int		ft_check_dup(int argc, char **argv)
{
	int		i;
	int		j;
	char	*num;

	i = 1;
	while (i < argc)
	{
		num = argv[i];
		j = i;
		while (j < argc)
		{
			if (j != i && ft_strcmp(num, argv[j]) == 0)
				return (0);
			j++;
		}
		i++;
	}
	return (1);
}

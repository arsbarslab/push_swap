/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rrr.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ttreutel <ttreutel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/27 17:36:21 by ttreutel          #+#    #+#             */
/*   Updated: 2019/01/28 23:11:36 by ttreutel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	ft_swap(t_stack **head)
{
	t_stack *tmp;

	if (!head || !(*head))
		return ;
	tmp = (*head)->next;
	if (!tmp)
		return ;
	(*head)->next = (*head)->next->next;
	tmp->next = *head;
	*head = tmp;
}

void	ft_rotate(t_stack **head)
{
	t_stack *tmp;

	if (!head || !(*head))
		return ;
	tmp = *head;
	while (tmp->next)
		tmp = tmp->next;
	tmp->next = *head;
	*head = (*head)->next;
	tmp->next->next = NULL;
}

void	ft_rev_rotate(t_stack **head)
{
	t_stack *tmp;
	t_stack *next;

	if (!head || !(*head) || !(*head)->next)
		return ;
	tmp = *head;
	while (tmp->next->next)
		tmp = tmp->next;
	next = tmp->next;
	tmp->next = NULL;
	next->next = *head;
	*head = next;
}

void	ft_rrr(t_stack **head_a, t_stack **head_b)
{
	ft_rev_rotate(head_a);
	ft_rev_rotate(head_b);
	ft_putstr("rrr\n");
}

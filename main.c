/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ttreutel <ttreutel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/27 17:41:08 by ttreutel          #+#    #+#             */
/*   Updated: 2019/01/28 20:03:31 by ttreutel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int			main(int argc, char **argv)
{
	t_stack *head_a;
	t_stack *head_b;

	if (argc == 1)
		return (0);
	head_b = NULL;
	head_a = ft_load_stack(argc, argv);
	if (!head_a)
		return (0);
	if (ft_is_sorted(head_a))
		return (0);
	while (ft_len_stack(head_a) > 3 && !ft_is_sorted(head_a))
		ft_pb(&head_b, &head_a);
	if (ft_len_stack(head_a) == 3)
		ft_solve_mini(&head_a);
	while (head_b)
		ft_solve(&head_a, &head_b);
	ft_to_begin(&head_a);
	ft_stiter(head_a, ft_clean);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ttreutel <ttreutel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/27 17:33:03 by ttreutel          #+#    #+#             */
/*   Updated: 2019/01/28 23:11:37 by ttreutel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"
#include "libft/get_next_line.h"

int		ft_read_cmd(t_stack **a, t_stack **b, char **cmd)
{
	int	command_id;

	if ((command_id = ft_is_correct(*cmd)))
	{
		ft_do_cmd(command_id, a, b);
		return (1);
	}
	else
	{
		ft_stiter(*a, ft_clean);
		ft_stiter(*b, ft_clean);
		return (0);
	}
}

void	ft_three(int argc, char **argv)
{
	int i;

	argv = ft_strsplit(argv[argc == 3 ? 2 : 1], ' ');
	argc = 0;
	while (argv[argc])
		argc++;
	if (!ft_check_num(argc, argv) || !ft_check_dup(argc, argv)
		|| !ft_is_bigger_int(argc, argv))
	{
		i = 0;
		while (i < argc)
		{
			ft_strdel(&(argv[i]));
			i++;
		}
		free(argv);
		ft_error();
	}
}

void	ft_check_in(int argc, char **argv, int *flag)
{
	if (argc == 1)
		exit(0);
	*flag = ft_cv(argv);
	if (*flag && argc == 3)
		ft_three(argc, argv);
	else if (argc == 2)
		ft_three(argc, argv);
	else if (!ft_check_num(argc, argv) || !ft_check_dup(argc, argv)
		|| !ft_is_bigger_int(argc, argv))
		ft_error();
}

void	ft_sotrted_check(t_stack *a, t_stack *b)
{
	if (ft_is_sorted(a))
	{
		ft_putstr("OK\n");
		ft_clean(a);
	}
	else
	{
		ft_putstr("KO\n");
		ft_clean(a);
		ft_clean(b);
	}
}

int		main(int argc, char **argv)
{
	t_stack	*a;
	t_stack	*b;
	char	*command;
	int		flag;

	ft_check_in(argc, argv, &flag);
	a = ft_load_stack(argc, argv);
	b = NULL;
	while (ft_get_next_line(0, &command) > 0)
	{
		if (!ft_read_cmd(&a, &b, &command))
			ft_error();
		if (flag)
			ft_print_stack(a, b);
	}
	ft_sotrted_check(a, b);
	return (0);
}

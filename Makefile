LIB = libft/libft.a
SRCP = ft_rotate.c ft_stack.c ft_swap_push.c ft_validate_input.c get_pos.c main.c move.c rrr.c solve.c to_begin.c check_flags.c stack.c move_pos.c
OBJP = $(SRCP:.c=.o)
SRCC = checker.c ft_rotate.c ft_stack.c ft_swap_push.c ft_validate_input.c move.c rrr.c check_flags.c commands.c get_pos.c solve.c to_begin.c ft_check.c stack.c move_pos.c
OBJC = $(SRCC:.c=.o)
CFLAGS = -Wall -Wextra -Werror
CC = gcc

all: $(LIB) push_swap checker

push_swap: $(OBJP)
	$(CC) $(CFLAGS) $(OBJP) -L libft -lft  -o push_swap
checker: $(OBJC)
	$(CC) $(CFLAGS) $(OBJC) -L libft -lft  -o checker
$(LIB):
	make -C libft/ all
%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@ -I.
clean:
	make -C libft/ clean
	rm -f *.o
fclean: clean
	make -C libft/ fclean
	rm -f push_swap checker
re: fclean all



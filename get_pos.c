/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_pos.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ttreutel <ttreutel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/27 17:35:44 by ttreutel          #+#    #+#             */
/*   Updated: 2019/01/28 23:11:37 by ttreutel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	ft_get_num(t_stack *head, int index)
{
	int i;
	int num;

	i = 0;
	num = 0;
	while (head && i < index)
	{
		head = head->next;
		i++;
	}
	if (head)
		num = head->num;
	return (num);
}

int	ft_check_pos(t_stack *head, int num, int index)
{
	int st_len;
	int new_index;

	new_index = index;
	st_len = ft_len_stack(head);
	if (st_len == 0)
		return (new_index);
	if (index == st_len - 1)
	{
		if (num > ft_get_num(head, index))
			new_index = 0;
		return (new_index);
	}
	else
	{
		if (num > ft_get_num(head, index))
			new_index = index + 1;
		return (new_index);
	}
}

int	ft_get_diff_neg(int head, int num)
{
	int diff;

	diff = 0;
	if (head <= 0)
	{
		if (head <= num)
			diff = num - head;
		else
			diff = head - num;
	}
	else if (head > 0)
		diff = head - num;
	return (diff);
}

int	ft_get_diff_pos(int head, int num)
{
	int diff;

	diff = 0;
	if (head >= 0)
	{
		if (head <= num)
			diff = num - head;
		else
			diff = head - num;
	}
	else if (head < 0)
		diff = num - head;
	return (diff);
}

int	ft_get_closest(t_stack *head, int num)
{
	int diff;
	int index;
	int tmp;

	diff = 2147483647;
	index = 0;
	tmp = 0;
	while (head)
	{
		if (num <= 0 && diff > ft_get_diff_neg(head->num, num))
		{
			diff = ft_get_diff_neg(head->num, num);
			index = tmp;
		}
		else if (num > 0 && diff > ft_get_diff_pos(head->num, num))
		{
			diff = ft_get_diff_pos(head->num, num);
			index = tmp;
		}
		tmp++;
		head = head->next;
	}
	return (index);
}
